#!/usr/bin/env python
from __future__ import division
from scipy import sparse
from sklearn.datasets import dump_svmlight_file
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
import argparse
import logging
import numpy as np
import os
import pandas as pd
import time



def generate_feature(train_file, test_file, app_event_file, app_label_file, event_file, label_file,
                     phone_file, train_feature_file, test_feature_file):
    """Generate features based on Smarth Agarwal's script."""
    logging.info('loading raw data files')
    trn = pd.read_csv(train_file, usecols=['device_id', 'group'], dtype={'device_id': np.str})
    tst = pd.read_csv(test_file, dtype={'device_id': np.str})

    logging.info('removeing gender and age from training, set groups to 0 for test')
    tst['group'] = 0

    logging.info('label-encoding group in training data')
    trn.group = LabelEncoder().fit_transform(trn.group)

    # Data - Events data
    # Bag of apps
    app_events = pd.read_csv(app_event_file, dtype={'device_id' : np.str})

    # remove duplicates(app_id)
    app_events = app_events.groupby("event_id")["app_id"].apply(
        lambda x: " ".join(set("app_id:" + str(s) for s in x))
    )

    events = pd.read_csv(event_file, dtype={'device_id': np.str})
    events["app_id"] = events["event_id"].map(app_events)
    events = events.dropna()
    events = events[["device_id", "app_id"]]

    # remove duplicates(app_id)
    events = events.groupby("device_id")["app_id"].apply(
        lambda x: " ".join(set(str(" ".join(str(s) for s in x)).split(" ")))
    )
    events = events.reset_index(name="app_id")

    # expand to multiple rows
    events = pd.concat([pd.Series(row['device_id'], row['app_id'].split(' '))
                        for _, row in events.iterrows()]).reset_index()
    events.columns = ['app_id', 'device_id']
    f3 = events[["device_id", "app_id"]]    # app_id

    app_labels = pd.read_csv(app_label_file)
    label_cat = pd.read_csv(label_file)
    label_cat = label_cat[['label_id','category']]

    app_labels = app_labels.merge(label_cat, on='label_id',how='left')
    app_labels = app_labels.groupby(["app_id","category"]).agg('size').reset_index()
    app_labels = app_labels[['app_id','category']]

    # Remove "app_id:" from column
    events['app_id'] = events['app_id'].map(lambda x : x.lstrip('app_id:'))
    events['app_id'] = events['app_id'].astype(str)
    app_labels['app_id'] = app_labels['app_id'].astype(str)

    events= pd.merge(events, app_labels, on = 'app_id',how='left').astype(str)

    # expand to multiple rows
    events= events.groupby(["device_id","category"]).agg('size').reset_index()
    events= events[['device_id','category']]

    f5 = events[["device_id", "category"]]    # app_id

    ##################
    #   Phone Brand
    ##################
    pbd = pd.read_csv(phone_file, dtype={'device_id': np.str})
    pbd.drop_duplicates('device_id', keep='first', inplace=True)

    ##################
    #  Train and Test
    ##################
    split_len = len(trn)

    # Concat
    Df = pd.concat((trn, tst), axis=0, ignore_index=True)

    Df = pd.merge(Df, pbd, how="left", on="device_id")
    Df["phone_brand"] = Df["phone_brand"].apply(lambda x: "phone_brand:" + str(x))
    Df["device_model"] = Df["device_model"].apply(lambda x: "device_model:" + str(x))

    ###################
    #  Concat Feature
    ###################
    f1 = Df[["device_id", "phone_brand"]]   # phone_brand
    f2 = Df[["device_id", "device_model"]]  # device_model

    f1.columns.values[1] = "feature"
    f2.columns.values[1] = "feature"
    f5.columns.values[1] = "feature"
    f3.columns.values[1] = "feature"

    FLS = pd.concat((f1, f2, f3, f5), axis=0, ignore_index=True)

    ###################
    # User-Item Feature
    ###################
    device_ids = FLS["device_id"].unique()
    feature_cs = FLS["feature"].unique()

    data = np.ones(len(FLS))
    len(data)

    dec = LabelEncoder().fit(FLS["device_id"])
    row = dec.transform(FLS["device_id"])
    col = LabelEncoder().fit_transform(FLS["feature"])
    sparse_matrix = sparse.csr_matrix(
        (data, (row, col)), shape=(len(device_ids), len(feature_cs)))

    sparse_matrix = sparse_matrix[:, sparse_matrix.getnnz(0) > 0]

    ##################
    #      Data
    ##################
    train_row = dec.transform(trn["device_id"])
    train_sp = sparse_matrix[train_row, :]

    test_row = dec.transform(tst["device_id"])
    test_sp = sparse_matrix[test_row, :]

    logging.info('saving as libSVM format')
    dump_svmlight_file(train_sp, trn.group, train_feature_file, zero_based=False)
    dump_svmlight_file(test_sp, tst.group, test_feature_file, zero_based=False)


if __name__ == '__main__':

    logging.basicConfig(format='%(asctime)s   %(levelname)s   %(message)s',
                        level=logging.DEBUG)

    parser = argparse.ArgumentParser()
    parser.add_argument('--train-file', required=True, dest='train_file')
    parser.add_argument('--test-file', required=True, dest='test_file')
    parser.add_argument('--app-event-file', required=True, dest='app_event_file')
    parser.add_argument('--app-label-file', required=True, dest='app_label_file')
    parser.add_argument('--event-file', required=True, dest='event_file')
    parser.add_argument('--label-file', required=True, dest='label_file')
    parser.add_argument('--phone-file', required=True, dest='phone_file')
    parser.add_argument('--train-feature-file', required=True, dest='train_feature_file')
    parser.add_argument('--test-feature-file', required=True, dest='test_feature_file')

    args = parser.parse_args()

    start = time.time()
    generate_feature(args.train_file,
                     args.test_file,
                     args.app_event_file,
                     args.app_label_file,
                     args.event_file,
                     args.label_file,
                     args.phone_file,
                     args.train_feature_file,
                     args.test_feature_file)
    logging.info('finished ({:.2f} sec elasped)'.format(time.time() - start))

