#!/usr/bin/env python

from __future__ import division
from sklearn.metrics import log_loss
from sklearn.svm import SVC

import argparse
import logging
import numpy as np
import os
import time

from const import N_CLASS, SEED
from kaggler.data_io import load_data


def train_predict(train_file, test_file, predict_valid_file, predict_test_file,
                  cv_id_file, 
                  C, kernel, tol, 
                  retrain=True, n_fold=5):

    feature_name = os.path.basename(train_file)[:-4]
    logging.basicConfig(format='%(asctime)s   %(levelname)s   %(message)s',
                        level=logging.DEBUG,
                        filename='svc_{}_{}_{}_{}.log'.format(C,
                                                          kernel,
                                                          tol,
                                                          feature_name))


    logging.info('Loading training and test data...')
    X, y = load_data(train_file, dense=True)
    X_tst, _ = load_data(test_file, dense=True)

    clf = SVC(C=C, kernel=kernel, probability=True, tol=tol, cache_size=2000, max_iter=-1, verbose=True, random_state=2016)

    logging.info('Loading CV Ids')
    cv_id = np.loadtxt(cv_id_file)

    logging.info('Cross validation...')
    P_val = np.zeros((X.shape[0], N_CLASS))
    P_tst = np.zeros((X_tst.shape[0], N_CLASS))
    for i in range(1, n_fold + 1):
        logging.info("cv %d" % i)
        i_trn = np.where(cv_id != i)[0]
        i_val = np.where(cv_id == i)[0]
        logging.debug('train: {}'.format(X[i_trn].shape))
        logging.debug('valid: {}'.format(X[i_val].shape))
        logging.debug(len(set(y[i_trn])))

        clf.fit(X[i_trn], y[i_trn])
        P_val[i_val] = clf.predict_proba(X[i_val])
        P_tst += clf.predict_proba(X_tst) / n_fold

        if not retrain:
            P_tst += clf.predict_proba(X_tst) / n_fold

    if retrain:
        logging.info('Retraining with 100% data...')
        clf.fit(X, y)
        P_tst = clf.predict_proba(X_tst)

    logging.info('Saving predictions...')
    np.savetxt(predict_valid_file, P_val, fmt='%.6f', delimiter=',')
    np.savetxt(predict_test_file, P_tst, fmt='%.6f', delimiter=',')


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--train-file', required=True, dest='train_file')
    parser.add_argument('--test-file', required=True, dest='test_file')
    parser.add_argument('--predict-valid-file', required=True,
                        dest='predict_valid_file')
    parser.add_argument('--predict-test-file', required=True,
                        dest='predict_test_file')
    parser.add_argument('--cv-id', required=True, dest='cv_id_file')
    parser.add_argument('--c', default=1.0, type=float, dest='C')
    parser.add_argument('--kernel', default='linear', type=str, dest='kernel')
    parser.add_argument('--tol', default=1e-3, type=float, dest='tol')
    parser.add_argument('--retrain', default=False, action='store_true')

    args = parser.parse_args()

    start = time.time()
    train_predict(train_file=args.train_file,
                  test_file=args.test_file,
                  predict_valid_file=args.predict_valid_file,
                  predict_test_file=args.predict_test_file,
                  cv_id_file=args.cv_id_file,
                  C=args.C,
                  kernel=args.kernel,
                  tol=args.tol,
                  retrain=args.retrain)
    logging.info('finished ({:.2f} min elasped)'.format((time.time() - start) /
                                                        60))
